## miniX 3 submission

### Loading

In this project i made a progress bar that keeps on getting further and further but also gets slower and slower and therefore will
always move towards the end but never get there since the numbers it uses to move gets lower and lower.

For this project I got inspired by the old tale and paradox of achilles and the slow tortoise, the idea of something or someone
constantly moving and yet can never get to the target was a fascinating idea, therefore i decided to make a progressbar that keeps loading 
and keeps moving but will never get to the end. I also added a console log command that makes you able to track the x value via the console.

I found a short ted-ed video on youtube to see the paradox on a video platform aswell as get my memory sharpened on how the 
paradox works. I also found a site that talked of the race between achilles and the tortoise, i decided to put a picture
of that in the program so that people who sees my program can make the connection between the progressbar and the old tale/timeparadox.

In the book The Techno-Galactic Guide to Software Observation Methods they write about how humans and computers experience time 
differently. Therefore i found it interesting to take this old time paradox from the ancient Greece and sorta translate it into 
code. New technology meets ancient history was an interesting concept to work with.
Under here is a picture of the project.

![](zeno2.PNG)


**sources**

https://www.youtube.com/watch?v=EfqVnj-sgcc

https://www.britannica.com/topic/Achilles-paradox

The Techno-Galactic Guide to Software Observation
Methods from the Techno-Galactic Software Observatory
(Brussels, June 2017)


**links**

Link for the program running on a webbrowser
https://vikingboi.gitlab.io/aestheticprogramming/miniX3/

Link for the code
https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX3/loading.js
