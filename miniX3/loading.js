var s = 1
var x = 119;
let img;
//Here i define two variables i will use later in the code
//And i define the picture so it can preload later

function preload(){
img = loadImage('zeno.jpg');
}
//This is the syntax for the picture i want to load into the program

function setup() {
createCanvas(1000,1000);
background(99,99,99);
image(img,205 ,50 );
fill(255,255,255)
rect (100,450,805,100,37)
}
//Here i create a canvas aswell as setting the background collur to grey
//I also sets the image to appear at cordinat 205,50
//The rectangle that is the progressbar is also made here and made white
function draw() {
fill(0,0,0)
ellipse(x, height/2, 38, 98);
s /= 1.00131;
x += 1*s;
console.log(x);
}
/*In this draw syntax i star by filling out the colour of the ellipse
with black.
The ellipse appears at the x cordinat which i defined at line 2. The y
position is half of the height in the canvas so basicly in the center.
The other numbers are the size og the ellipse.
Then the s value is divided by a small number 1.00131 this makes it so
the x variable which controls the length of the ellipse aka the progress
starts fast and gets slower and slower since it * by the s value which keeps
changing. This all repeats indifinelty cause it is in a draw function that
loops.
I also added a console log command so you can inspect what the x value is
when you inspect via comandconsole in the webbrowser.
*/
