## miniX 7 submission

### Smile revisit

In this week's miniX I have looked back at my previous project smile which was my submission in miniX2. I looked at the code and changed some small mistakes I noticied. For example I had in the code named the picture of the phone let img; but then I had assigned it in the preload function as img0 = loadImage('cam.PNG'); this did not do anything wrong in the end but if I programmed in another more advanced library than p5.js it would probably have been an error. In the program there were also some unnecessary curly brackets which I deleted. It is funny to look back and see how much better I have become in programing in just a few weeks

I changed the project a bit. I added two stars which appear in the emojis' eyes when a key is pressed and it tries to  look happy for the camera. I also based on the feedback I got changed the colour of the emoji from being green at all times to only being it when the camera is there and when the camera is gone it is its normal yellowish colour like the normal emojis we know from everyday life. This change is supposed to make it more clear the theme of the project that social media and the constant need to frame ourselves in a happy mood makes the concept of showing the real you online an alien concept hence the green colour. 

The deeper I have gotten into the course aestheticprogramming I begin to understand the digital culture in a different way both in terms of scale and impact but also how you can make art and different works that can shape the digital culture. It becomes more clear to me how aestheticprogramming can be used as a way of thinking and doing in regards to the digital culture which we live in today. It also gives a way to understand some of the complex procedures that dictate our daily lives. 

![](smiley1.PNG)

_**This is the first emoji you see when you enter the program in the original miniX2 it had a green colour.**_

![](smiley2.PNG)

_**This is the emoji you see when you hold down a key, in the original it was also green but now I have added stars in the eyes, to further describe the point of the work that we become someone else when the internet or socialmedia is present to us.**_

**sources**

https://www.nicepng.com/ourpic/u2q8q8t4e6w7a9w7_star-shine-png-banner-black-and-white-library/  (the star)

Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70



**links**

Link for the program running on a webbrowser

https://vikingboi.gitlab.io/aestheticprogramming/miniX7/


Link for the code

https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX7/smile.js
