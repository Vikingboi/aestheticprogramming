let img;
let star;

function preload(){
img = loadImage('cam.PNG');
star = loadImage('star.png');
}
//Here i preload and define my image i use in the program

function setup() {
createCanvas(800,800);
background(0,0,255);
}
//Here i put my setupcode that creates the canvas and background

function draw(){
if (keyIsPressed == true){
//This function loads the camera picture when a key is pressed and the
//two stars aswell
ellipse(400,400,250,250);
fill(0,0,0);
ellipse(350,350,35,80);
fill(0,0,0);
ellipse(450,350,35,80);
fill(255,71,30);
arc(402,450,50,45,6,173,PIE);
fill(0,255,0);
image(img, 0, 0);
image(star,325,325,52,52)
image(star,425,325,52,52)
}
//Here it draws 3 diffrent ellipses and an arc which makes a smiley face : )

else
{
background(0,0,255);
ellipse(400,400,250,250);
fill(0,0,0);
ellipse(350,350,35,80);
fill(0,0,0);
ellipse(450,350,35,80);
fill(255,71,30);
textSize(80);
text('Hold any key', 170, 700);
arc(402,470,55,50,173,6,PIE);
fill(255,245,0);
}
}
/* Here i made an else statement that is active if no key is pressed
the arch is changed so it resembels a sad smileys mouth insted of a happy one
And the color is yellow when a key is not held down but turns green when
a key is pressed.
*/
