let img;
let speed = 5;
let x = 100;
let y = 505;
let t = 99;
let spikeArr = [];
let d;
let d1;
let d2;
let d3;
let d4;
let f;
let sound;
let soundw;
let j;
let jt;
//Up here I define my variables so i can refer to them later

function preload(){
img = loadImage('keensprite.PNG');
soundFormats('mp3');
sound = loadSound('keensound.mp3');
soundFormats('mp3');
soundw = loadSound('victory.mp3');
}
//I preload the image of the game character and the two soundfiles

function setup(){
  createCanvas(1300,600);
  background(0,0,198);
  frameRate(30);

  for(let i = 0; i < 5; i++){
    spikeArr[i] = new Spike(700,601,730,601,715,555);
    spikeArr[0] = new Spike(630,601,610,601,620,555);
    spikeArr[1] = new Spike(940,601,910,601,925,580);
    spikeArr[2] = new Spike(940,601,970,601,955,581);
    spikeArr[3] = new Spike(970,601,1000,601,985,582);
  }
}

//Up here i have used an array to make a lot of spikes appear at set cordinates
//I have used a class to make the triangles/spikes appear

function draw(){
background(0,0,198);
image(img,x,y);
move();
t +=1.9
jt +=1
//Here I draw and make my img move asweel as making a timer

for(let i = 0; i < 5; i++){
  spikeArr[i].show();
}
//up here i say that alle the objects in the array must now show

d = dist(620,555,x+38,y+95);

d1 = dist(715,555,x+38,y+95);

d2 = dist(925,594,x+38,y+95);

d3 = dist(955,595,x+38,y+95);

d4 = dist(985,596,x+38,y+95);
//Here I have a point that tracks how close that point is to the character
//commander Keen

if(y<509){
y += 4
}
//Here I have made gravity the 510 is the point on the y axis keen will stop
//Falling with the speed 1.8 pixels

fill(255,0,0);
triangle(622,565,618,565,620,550);
triangle(712,565,719,565,715,553);
triangle(920,586,930,586,925,577);
triangle(951,586,959,586,955,578);
triangle(982,586,989,586,985,579);
//Here i have made tiny red triangles on top of the black ones

if(x>1150){
  noLoop();
  fill(0,255,0)
  textSize(95);
  text('NICEJOB!',400,300);
  soundw.play()
}
/*Up here I have made something trigger when the x value is above 1150 the x
value moves up when keen moves to the right. When this event triggers the
code stoops looping and a text and sound appear.
*/

if(d<9){
  noLoop();
  textSize(100);
  background(0);
  text('GAMEOVER',320,300);
  sound.play()
}
if(d1<9){
  noLoop();
  textSize(100);
  background(0);
  text('GAMEOVER',320,300);
  sound.play()
}
if(d2<12){
  noLoop();
  textSize(100);
  background(0);
  text('GAMEOVER',320,300);
  sound.play()
}
if(d3<12){
  noLoop();
  textSize(100);
  background(0);
  text('GAMEOVER',320,300);
  sound.play()
}
if(d4<12){
  noLoop();
  textSize(100);
  background(0);
  text('GAMEOVER',320,300);
  sound.play()
}
/*Here I have made if statements that say if the distance between Keen and
the points on the spikes is low enough it will stop looping and make a text
appear that says GAMEOVER aswell as change the background and play a soundfile
*/

textSize(30);
fill(0, 0, 0);
text('Next level --',1080,500);
text('>',1240,503);
//Here I have made a text appear with at the far right of the scrren to guide
//the user.
if(j==true){
  y -= 13
  jt = 0
}

if(jt<200){
  j = false
}

if(y<430){
  t=0
}
/*Here are some if statements that make sure that when keen has reached his
max height he falls down and cant jump for a little while
*/
}

function move(){
  if(t>50){
    if(keyIsDown(UP_ARROW)){
    j=true
    }
  }

  if(keyIsDown(RIGHT_ARROW)){
    x+=speed;
}
  if(keyIsDown(LEFT_ARROW)){
    x-=speed;
  }
}
//Here I have made the movment commands that moves the character forward
//and upwards

class Spike{
  constructor(_x,_y,_x2,_y2,_x3,_y3){
  this.x = _x
    this.y = _y
    this.x2 = _x2
      this.y2 = _y2
      this.x3 = _x3
        this.y3 = _y3
    }
    show(){
      triangle(this.x, this.y, this.x2, this.y2, this.x3, this.y3)
    }
  }
//Down here i have made a class which makes the spikes into objects so I can
//easily refer to them and make more of them appear they have values in the
//form of cordinates
