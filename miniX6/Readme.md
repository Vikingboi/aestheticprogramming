## miniX 6 submission

### Commander Keen mod(sv)

In this week's miniX I have made a new version of an old game that I really liked and enjoyed in my childhood. The game series is called Commander Keen. My father showed me the game and we loved to play it and share that experience together, even though it was a singleplayer game. I find it fascinating that a singleplayer game can have an effect beyond just the one person who bought it, and can be an experience that you can share with others. The objective in the game is to get to the end of the level and avoid all dangers along the way. 
 
This week we learned about object oriented Programming and how we can make it in code. The objects or classes which it is called in p5 can be useful in different aspects, for example if you need to make a lot of the same type of enemies in a game you can easily refer to the object and then all the properties and methods it has will be copied over. In my program I have made it so the spikes that Commander Keen can touch and get a gameover screen by hitting are all a class. So the spikes class makes it so I can call that object to show() and then it will appear on the canvas in the different places I want it to. 
It is also relevant to think about that when I program and load an image in and make it move according to the arrow keys it is also registered as an object in the code automatically, that's how basically all the coding we do nowadays work on the computer.
 
The game commander keen has a following of dedicated fans and some of them have made modified versions of the game also called mods. So some fans have made a game just like the other commander keen games and looks very similar, so even if the company id Software who made the series does not make new games in the series anymore the fans can still enjoy these fan made mods totally free. 
 
In order to program a game like this the gamecharter/object Keen, needs to be abstracted to some basic values like his height, looks, jump height,lives etc. So everything "irelevant" information is not included in the programming, for example gender. The reason we call Keen a boy is because of the way the game charter is designed, and how the story is told in the game so we get to know this charter a bit.
But the first commander keen game came out in 1990 and a lot has happened since then. Today, since digital culture is such a huge part of our society, the way we represent people in video games and other media can have an impact in regards to how we look at and understand other people and ourselves.
 
PS in the spirit of the Commander keen games which liked to have small hidden paths and secrets in them. I have made a secret way to get past the first two spikes. Can you find it? ; )

![](modpicture.PNG)


**sources**


Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

https://www.youtube.com/watch?v=eX1TjYv3dfc    (Source for the victory sound)

https://en.wikipedia.org/wiki/Commander_Keen    (Source for the gameseries)


**links**

Link for the program running on a webbrowser

https://vikingboi.gitlab.io/aestheticprogramming/miniX6/


Link for the code

https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX6/keen.js

