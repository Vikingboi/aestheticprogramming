let button;
let ctracker;
let capture;
var s = 1

// Up here i define some variables 3 that make the facial and video capturing
// posible and one variable which will be used to make a timer later.

function setup() {
  createCanvas(640,480);
  button = createButton('BIG BROTHER IS WATCHING YOU');
  button.style("color","#ff0000");
  button.style("background","#171717");
  button.size(300,90)
  button.position(50,50);
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
}

/* Up here i have used all the variables from the beginings except for s
in the setup command i create the button aswell as the style and size.
The capturing and tracking is also set up here and the size is defined.
*/

function draw() {

image(capture, 0,0, 640, 480);

let positions = ctracker.getCurrentPosition();
if (positions.length)
//checks the availability of web cam tracking

{ button.position(positions[20][0]-100, positions[20][1]-110);

/* Up here i decide which point of the face it needs to place the
button. I have subtracted with 100 and 110 in order to get the button
to appear over a persons head. */

for (let i = 0; i < positions.length; i++) {
if (s<100) {
textSize(32);
text('Scan youre face citizen! glory to Oceania!', 21, 400);
fill(255, 0, 0);
       } else {
       }
s += 0.004;
}
}
}
/* Up here i have made a timer aswell as a text that goes away according to
that timer. I made an if else syntax which says that if the variable s is
smaller than 100 it will draw the text and after that it will plus the s
value with 0.004 that way the variable s constantly gets bigger since
in a draw function it repeats itself indifinelty. The s value is set to
start at 1 so the text will show when the program starts.
*/
