## miniX4 submission

**BigBrother**

In this week's miniX I made a program that uses the computer's camera to track the user's face and then put a text above the head of the user that says "BIG BROTHER IS WATCHING YOU". There is also a text that says "Scan youre face citizen! glory to Oceania!" this one is static and goes away after a couple of seconds. I included this line in order to guide the user in what to do, to get the button over their head like i intended. The line also fits the theme of the book Nineteen Eighty-Four, so it is an instruction that doesn't take the user out of the intended feel and experience.  

This project is heavily inspired by the book Nineteen Eighty-Four written by George Orwell, a book that has fairly accurately predicted what direction technology and surveillance would take in the future which i find fascinating. 

In the project I removed the red dots that usually follow the face via the tracker, because it didn't track good enough and did not always get all the points on the face correcty, so i decided to removed/hide them. Of course the program still tries to put it on the users face, but now the only thing we can see is the text "Scan youre face citizen! glory to Oceania!" and the button "BIG BROTHER IS WATCHING YOU" the button follows the head of the user pretty well, so therefore I decided the red dots tracking the face wasent needed and could be hidden away. And the program would run even more smoothly withou them.  

The text from Data capture made me think about what kind of data can be taken from us and how invisible that process is, all this data that is collected on us today, is close to the same way "the party" operates in the book Nineteen Eighty-Four. This got me inspired to make my project like the way society works in Oceania. And If a simple programing beginner/newbie like myself can set this program up like "the party" has done in George Orwell's book, then what can companies and governments do with all this technology we have today? and should they have so much power?

![](Udklipbrother.PNG)

**sources**

Orwell, G. Nineteen Eighty-Four

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

**links**

Link for the program running on a webbrowser
https://vikingboi.gitlab.io/aestheticprogramming/miniX4/

Link for the code
https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX4/bigbrother.js
