## miniX2 submission


### Project smile

In this project I made two emojis, a sad emoji and a happy emoji. The project starts with a sad smiley face
that can change into a happy face when you hold or press a button. When the user does this a camera appears as well. 
In my last miniX i got feedback regarding that it was hard to know what you needed to do in order to interact with 
the project. So this time i made a text box appear under the emoji that says "Hold any key" this guides the user 
to get the full experience by not just clicking but holding down a key. 

I have tried to make a critical design project, the intention behind this coding/project is to show how we people
display fake personas and emotions on social media and when people are watching us. In society we have developed a
culture on socialmedia that only wants to see the good side from us instead of how we actually feel.
This i try to convey in the form of the emoji changing from sad to happy when the camera appears
because now it is not alone and need to show its "goodside" so to speak.

![](emoji.PNG)


Link for the program running on a webbrowser
https://vikingboi.gitlab.io/aestheticprogramming/miniX2/

Link for the code
https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX2/smile.js
