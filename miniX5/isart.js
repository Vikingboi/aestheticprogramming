var t = 2
//Up here i define a variable that will be used later to make a timer

function setup(){
createCanvas(windowWidth,windowHeight)
background(255,255,255);
frameRate(20);
}
/*I use the setup function to make the canvas the size of the users
windowWidth and windowHeight as weel as setting the background color
and framerate
*/

function draw() {
fill(255,255,255);
noStroke();
circle(random(windowWidth),random(windowHeight),80);
fill(255,0,0);
noStroke();
circle(random(windowWidth),random(windowHeight),130);
textSize(34);
fill(255, 255, 255);
text('      is it still art', windowWidth/2, windowHeight/2);
text('if I show you the code?', windowWidth/2, windowHeight/2+30);
/*Up here I have put some diffrent functions inside the draw function
it makes a white circle everytime it loops aswell as a red one.
It picks a random number for the x and y axis based on the windowWidth
and windowHeight of the user. There is also two texts that gets drawn.
*/

if(t==200){
fill(255,0,0);
circle(windowWidth/2+185, windowHeight/2,360)
fill(255, 255, 255);
text('      is it still art', windowWidth/2, windowHeight/2);
text('if I show you the code?', windowWidth/2, windowHeight/2+30);
}
t += 1;
console.log(t);
}

/*Up here i have made a timer in the draw function that activates when the
variable t hits 200 and it pluses with 1 each time the draw function loops
t starts at the value 2. When the timer is set off it makes a big red circle
at the coridinate where the text is so it can be seen more easily the texts
also draws again so it gets in the front of the red circle.
*/
