## miniX5 submission

**Is it art?**

In this week's minix our task was to design a program that uses some simple rules that then can make some complex or interesting patterns. Another theme I found interesting in this week's course was randomness and how nothing really is truly random. 

In regards to my code i have three simple rules that makes the whole sketch function

Rule 1 Each time the code loops (that's 20 times a second with the framerate set to 20) draw a white circle with a diameter of 80 pixels at a random coordinate.

Rule 2 Each time the code loops (again that's 20 times a second with the framerate set to 20)  draw a red circle with a diameter of 130 pixels at a random coordinate. 

Rule 3 When the variable t is == 200 then draw a red circle with a diameter of 360 on the text. Is it still art if I show you the code? this process does not repeat. 

When you see the program running for the first time, it is hard to notice that I have tampered with the randomness, so the red circles have an easier time showing the text since the white circles, which hide the text, are smaller than the red circles. When you look at the rules you can see the red circles are bigger than the white ones so it is not truly random what color the majority of the screen will turn.

In rule 3 I made a big red circle appear on the text so it would be easier to see it. Because the circle appears later it becomes sort of hidden that it appears at all since there are already a lot of red circles on the canvas, so people may not notice this 3 rule taking effect when so much else is happening on the screen.
The text in the program is a critical take on another theme we had this week, regarding what is art? when developing something like this. In regards to that debate I added the text, “is it still art if I show you the code?”  This line questions if it is the idea of us not being able to recreate something that makes it art? or is it something else?

Here is a picture of the program : )

![](isart.PNG)


**sources**

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

**links**

Link for the program running on a web browser 
https://vikingboi.gitlab.io/aestheticprogramming/miniX5/

Link for the code 
https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX5/isart.js

