## miniX1 submission


### Project random dice generator

In this Project I made a dice appear randomly on the screen
every time a button is pressed. It works just like you would expect
you press a button and you get a random dice ranging from 1-6.
The dice also work when you hold down a key instead of just pressing once
this gives you a feeling of you are shaking the dice.
I have made these

pictures in atom and then I took a screenshot of the 6 different dice and put
them into my miniX1 folder. Since I took the screenshots they weren't perfect
there was some small white lines on the edges of the dice. However these white
lines cannot be seen in the program since the background is also white.

The dice being a bit different in the way they are clipped gives the dice a
shaky felling to it when you hold down a key in the program.
I think it is an important lesson that not every "mistake" in the code or
picture etc. is visible in the end project and maybe instead of using a lot of
time to fix a small problem it may be easier to hide it or embrace it and turn
it into somethin useful and fun : )

Here is a picture from the project and the code used for making
the different dice.

![](dice6.PNG)

function setup() {
  createCanvas(500,500);
  background(0,0,255);
}

function draw() {
  ellipse(100,100,100,100);
  ellipse(100,400,100,100);
  ellipse(400,100,100,100);
  ellipse(100,250,100,100);
  ellipse(400,250,100,100);
  ellipse(400,400,100,100);
}

Link for program running on a webbrowser
https://vikingboi.gitlab.io/aestheticprogramming/miniX1/

Link for the code
https://gitlab.com/Vikingboi/aestheticprogramming/-/blob/main/miniX1/randomgen.js
